// Core
import { render } from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';

// Instruments
import { RoutesComponent } from './navigation';
import { store } from './lib/redux/init/store';

// Styles
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import './theme/index.scss';

render(
    <Provider store = { store }>
        <Router>
            <RoutesComponent />
        </Router>
    </Provider>,
    document.getElementById('root'),
);
