/* Other */
import { ILogin, IProfile } from '../types';
import { useAppDispatch } from '../lib/redux/init/store';
import { authActions } from '../lib/redux/actions/auth';

export const useActWithUser = () => {
    const dispatch = useAppDispatch();

    const loginUser = (user: ILogin) => {
        const { email, password } = user;
        const auth = `${window.btoa(`${email}:${password}`)}`;

        dispatch(authActions.setLoginUser(auth));
    };

    const signUpUser = (user: IProfile) => {
        dispatch(authActions.setSignUpUser(user));
    };

    const logout = (token: string | null) => {
        dispatch(authActions.logoutUser(token));
    };

    const setInformUser  = (token: string) => {
        dispatch(authActions.setUser(token));
    };

    const updateUser = (user: IProfile, token: string | null) => {
        dispatch(authActions.setUpdateUser(user, token));
    };

    return {
        loginUser, signUpUser, logout, setInformUser, updateUser,
    };
};
