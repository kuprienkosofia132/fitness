import { useAppDispatch, useAppSelector } from '../lib/redux/init/store';
import { getToken } from '../lib/redux/selectors/auth';
import { trackersActions } from '../lib/redux/actions/tracker';

export const useDeleteRecord = () => {
    const dispatch = useAppDispatch();
    const token: string | null = useAppSelector(getToken);

    const deleteDailyRecord = () => {
        dispatch(trackersActions.removeAllRecord(token));
    };

    return { deleteDailyRecord };
};
