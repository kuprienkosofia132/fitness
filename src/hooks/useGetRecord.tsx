/* Other */
import { useAppDispatch } from '../lib/redux/init/store';
import { trackersActions } from '../lib/redux/actions/tracker';

export const useGetRecord = () => {
    const dispatch = useAppDispatch();

    const record = (type: string, token: string | null) => dispatch(trackersActions.getRecord(type, token));

    return { record };
};
