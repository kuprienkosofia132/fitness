/* Other */
import { useAppDispatch } from '../lib/redux/init/store';
import { trackersActions } from '../lib/redux/actions/tracker';

export const useGetScore = () => {
    const dispatch = useAppDispatch();

    const getDailyScore = (token: string | null) => {
        dispatch(trackersActions.getScoreTracker(token));
    };

    return { getDailyScore };
};
