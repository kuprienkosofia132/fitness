/* Other */
import { useAppDispatch } from '../lib/redux/init/store';
import { trackersActions } from '../lib/redux/actions/tracker';
import { IRecord } from '../types';

export const useUpdateRecord = () => {
    const dispatch = useAppDispatch();

    const updateRecord = ({ type, record }: IRecord, hash: string, token: string | null) => {
        dispatch(trackersActions.updateRecord({ type, record }, hash, token));
    };

    return { updateRecord };
};
