// Core
import { FC, ReactElement, useEffect } from 'react';
import cx from 'classnames';
import { useLocation } from 'react-router-dom';

// Book
import { book } from '../../navigation/book';

// Styles
import Styles from './styles/index.module.scss';

// Elements
import { Spinner } from '../../elements/spinner';

// Hooks
import { useAppSelector } from '../../lib/redux/init/store';
import { getRecord, getScore } from '../../lib/redux/selectors/tracker';
import { getProfile, getStatusLoading, getToken } from '../../lib/redux/selectors/auth';
import { Header } from './header';
import { useGetScore } from '../../hooks/useGetScore';

export const Base: FC<IPropTypes> = (props) => {
    const authUser = useAppSelector(getProfile);
    const path = useLocation();
    const token = useAppSelector(getToken);
    const { getDailyScore } = useGetScore();
    const score = useAppSelector(getScore);
    const tracker = useAppSelector(getRecord);
    const isLoading = useAppSelector(getStatusLoading);

    useEffect(() => {
        if (token) {
            getDailyScore(token);
        }
    }, [tracker.value]);

    const {
        children,
        center,
        disabledWidget,
    } = props;

    const avatarCX = cx([
        Styles.sidebar, {
            [ Styles.male ]:   authUser.sex === 'm',
            [ Styles.female ]: authUser.sex === 'f',
        },
    ]);

    const contentCX = cx(Styles.content, {
        [ Styles.center ]: center,
    });

    const loaderCX = <Spinner isLoading = { isLoading } />;

    const widgetJSX = score !== null && !disabledWidget && (
        <div className = { Styles.widget }>
            <span className = { Styles.title }>Life Score</span>
            <div className = { Styles.module }>
                <span className = { Styles.score } style = { { bottom: `${score * 0.5}%` } }>{ score * 0.5 }</span>
                <div className = { Styles.progress }>
                    <div className = { Styles.fill } style = { { height: `${score * 0.5}%` } } />
                </div>
                <span className = { cx([Styles.label, Styles.level1]) }>Off Track</span>
                <span className = { cx([Styles.label, Styles.level2]) }>Imbalanced</span>
                <span className = { cx([Styles.label, Styles.level3]) }>Balanced</span>
                <span className = { cx([Styles.label, Styles.level4]) }>Healthy</span>
                <span className = { cx([Styles.label, Styles.level5]) }>Perfect Fit</span>
            </div>
        </div>
    );

    return (
        <section className = { Styles.profile }>
            <div className = { avatarCX }>
                { loaderCX }
            </div>
            <div className = { Styles.wrap }>
                <div className = { Styles.header }>
                    <Header />
                </div>
                <div className = { contentCX }>
                    { children }
                    { path.pathname !== book.profile.url ? widgetJSX : null }
                </div>
            </div>
        </section>
    );
};

interface IPropTypes {
    children: ReactElement | ReactElement[];
    center?: boolean;
    disabledWidget?: boolean;
}
