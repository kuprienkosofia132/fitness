// Core
import {
    Link, matchPath, useLocation,
} from 'react-router-dom';
import cx from 'classnames';
import { useEffect } from 'react';

// Styles
import Styles from './styles/index.module.scss';
import StylesUser from '../../elements/user/styles.module.scss';

// Book
import { book } from '../../navigation/book';

// Hooks
import { useActWithUser } from '../../hooks/useActWithUser';

/* Other */
import { useAppSelector } from '../../lib/redux/init/store';
import { getProfile, getToken } from '../../lib/redux/selectors/auth';

export const Header = () => {
    const authUser = useAppSelector(getProfile);
    const token = useAppSelector(getToken);
    const { logout } = useActWithUser();
    const { setInformUser } = useActWithUser();

    useEffect(() => {
        if (token) {
            setInformUser(token);
        }
    }, [token]);

    const { pathname } =  useLocation();

    const isExact = matchPath(book.root.url, pathname);

    const avatarCX = cx({
        [ StylesUser.male ]:   authUser.sex === 'm',
        [ StylesUser.female ]: authUser.sex === 'f',
    });

    const homeLinkJSX = !isExact && (
        <Link to = { book.root.url } className = { Styles.homeLink }>На главную</Link>
    );

    return (
        <>
            <div> { homeLinkJSX } </div>
            <div className = { `${StylesUser.avatar} ${avatarCX}` }>
                <div className = { StylesUser.column }>
                    <Link to = '/profile' className = { StylesUser.name }>{ `${authUser.fname} ${authUser.lname}` }
                    </Link>
                    { token && <button
                        onClick = { () => {
                            logout(token);
                            // navigate('/login');
                        }
                        } className = { StylesUser.logout }>Выйти</button> }
                </div>
            </div>
        </>
    );
};
