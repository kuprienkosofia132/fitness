// Core
import { FC } from 'react';

// Components
import { DashboardFront } from '../../bus/tracker/components/dashboard/DashboardFront';

// Views
import { Base } from '../../views/base';

export const Dashboard: FC = () => {
    return (
        <>
            <Base>
                <DashboardFront />
            </Base>
        </>
    );
};
