export const typesAuth = Object.freeze({
    SET_TOKEN:      'SET_TOKEN',
    SET_USER:       'SET_USER',
    RESET_TOKEN:    'RESET_TOKEN',
    UPDATE_USER:    'UPDATE_USER',
    LOGOUT_USER:    'LOGOUT_USER',
    SET_ERROR:      'SET_ERROR',
    START_FETCHING: 'START_FETCHING',
    STOP_FETCHING:  'STOP_FETCHING',
    RESET_ERROR:    'RESET_ERROR',
});
