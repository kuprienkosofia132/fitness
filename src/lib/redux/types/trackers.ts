
export const typesTrackers = Object.freeze({
    SET_RECORD:     'SET_RECORD',
    GET_RECORD:     'GET_RECORD',
    UPDATE_RECORD:  'UPDATE_RECORD',
    RESET_RECORD:   'RESET_RECORD',
    DELETE_REPORTS: 'DELETE_REPORTS',
    GET_SCORE:      'GET_SCORE',
});
