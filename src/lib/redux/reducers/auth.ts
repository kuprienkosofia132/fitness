import { AnyAction } from 'redux';
import { typesAuth } from '../types/auth';

const initialState = {
    token:        '',
    errorMessage: '',
    error:        false,
    user:         {},
    isLoading:    false,
};

export const AuthReducer = (state = initialState, action: AnyAction) => {
    switch (action.type) {
        case typesAuth.SET_TOKEN: {
            return {
                ...state,
                token:        action.payload,
                error:        false,
                errorMessage: '',
            };
        }
        case typesAuth.START_FETCHING: {
            return {
                ...state,
                isLoading: true,
            };
        }
        case typesAuth.STOP_FETCHING: {
            return {
                ...state,
                isLoading: false,
            };
        }
        case typesAuth.SET_USER: {
            return {
                ...state,
                user:         action.payload,
                error:        false,
                errorMessage: '',
            };
        }
        case typesAuth.LOGOUT_USER: {
            return {
                ...state,
                token:        action.payload,
                user:         {},
                error:        false,
                errorMessage: '',
            };
        }
        case typesAuth.UPDATE_USER: {
            return {
                ...state,
                user:         action.payload,
                error:        false,
                errorMessage: '',
            };
        }
        case typesAuth.SET_ERROR: {
            return {
                ...state,
                errorMessage: action.payload,
                error:        true,
            };
        }
        case typesAuth.RESET_ERROR: {
            return {
                ...state,
                errorMessage: '',
                error:        false,
            };
        }
        default: {
            return {
                ...state,
            };
        }
    }
};
