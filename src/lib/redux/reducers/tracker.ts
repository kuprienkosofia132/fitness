import { AnyAction } from 'redux';
import { typesTrackers } from '../types/trackers';

const initialState = {
    error:        false,
    errorMessage: '',
    record:       {},
    score:        '',
};

export const trackerReducers = (state = initialState, action: AnyAction) => {
    switch (action.type) {
        case typesTrackers.SET_RECORD: {
            return {
                ...state,
                record:       action.payload,
                error:        false,
                errorMessage: '',
            };
        }
        case typesTrackers.RESET_RECORD: {
            return {
                ...state,
                record:       {},
                error:        false,
                errorMessage: '',
            };
        }
        case typesTrackers.UPDATE_RECORD: {
            return {
                ...state,
                record:       action.payload,
                error:        false,
                errorMessage: '',
            };
        }
        case typesTrackers.GET_RECORD: {
            return {
                ...state,
                record:       action.payload,
                error:        false,
                errorMessage: '',
            };
        }
        case typesTrackers.DELETE_REPORTS: {
            return {
                ...state,
                score:        '',
                record:       {},
                error:        false,
                errorMessage: '',
            };
        }
        case typesTrackers.GET_SCORE: {
            return {
                ...state,
                score:        action.payload,
                error:        false,
                errorMessage: '',
            };
        }
        default:
            return {
                ...state,
            };
    }
};
