import { RootState } from '../init/store';
import { IResponseRecord } from '../../../types';

export const getRecord = (state: RootState): IResponseRecord => {
    return state.tracker.record;
};

export const getScore = (state: RootState): number | null => {
    return state.tracker.score;
};
