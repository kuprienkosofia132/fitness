import { RootState } from '../init/store';
import { IProfile } from '../../../types';

export const getToken = (state: RootState): string => {
    return state.auth.token;
};
export const getErrorMessage = (state: RootState): string => {
    return state.auth.errorMessage;
};
export const getProfile = (state: RootState): IProfile => {
    return state.auth.user;
};
export const getStatusLoading = (state: RootState): boolean => {
    return state.auth.isLoading;
};
