/* Other */
import { typesAuth } from '../types/auth';
import { IProfile } from '../../../types';
import { AppThunk } from '../init/store';
import { users } from '../../../api/users';

export const authActions = Object.freeze({
    setToken: (token: string) => {
        return {
            payload: token,
            type:    typesAuth.SET_TOKEN,
            error:   false,
        };
    },
    resetToken: () => {
        return {
            payload: '',
            type:    typesAuth.RESET_TOKEN,
            error:   false,
        };
    },
    setError: (message: string) => {
        return {
            payload: message,
            type:    typesAuth.SET_ERROR,
            error:   true,
        };
    },
    resetError: () => {
        return {
            type: typesAuth.RESET_ERROR,
        };
    },
    setSignUpUser: (credentials: IProfile): AppThunk => async (dispatch) => {
        try {
            const token = await users.create(credentials);
            dispatch(authActions.setToken(token));
        } catch (error) {
            const { message } = error as Error;
            dispatch(authActions.setError(message));
        }
    },
    startFetching: () => {
        return {
            type: typesAuth.START_FETCHING,
        };
    },
    stopFetching: () => {
        return {
            type: typesAuth.STOP_FETCHING,
        };
    },
    setLoginUser: (credentials: string): AppThunk => async (dispatch) => {
        try {
            dispatch(authActions.startFetching());
            const token = await users.login(credentials);
            dispatch(authActions.setToken(token));
        } catch (error) {
            const { message } = error as Error;
            dispatch(authActions.setError(message));
        } finally {
            dispatch(authActions.stopFetching());
        }
    },
    logoutCurrentUser: () => {
        return {
            type:    typesAuth.LOGOUT_USER,
            payload: '',
        };
    },
    logoutUser: (token: string | null): AppThunk => async (dispatch) => {
        try {
            await users.logout(token);
            dispatch(authActions.logoutCurrentUser());
        } catch (error) {
            const { message } = error as Error;
            dispatch(authActions.setError(message));
        }
    },
    updateUser: (user: IProfile) => {
        return {
            payload: user,
            type:    typesAuth.UPDATE_USER,
            error:   false,
        };
    },
    setAuthUser: (user: IProfile) => {
        return {
            payload: user,
            type:    typesAuth.SET_USER,
            error:   false,

        };
    },
    setUser: (token: string | null): AppThunk => async (dispatch) => {
        try {
            dispatch(authActions.startFetching());
            const response = await users.getMe(token);
            dispatch(authActions.setAuthUser(response));
        } catch (error) {
            const { message } = error as Error;
            dispatch(authActions.setError(message));
        } finally {
            dispatch(authActions.stopFetching());
        }
    },
    setUpdateUser: (payload: IProfile, token: string | null): AppThunk => async (dispatch) => {
        try {
            dispatch(authActions.startFetching());
            const response = await users.updateMe(payload, token);
            dispatch(authActions.setAuthUser(response.data.data));
            // @ts-ignore
            dispatch(authActions.setToken(response?.data?.token));
        } catch (error) {
            const { message } = error as Error;
            dispatch(authActions.setError(message));
        } finally {
            dispatch(authActions.stopFetching());
        }
    },

});
