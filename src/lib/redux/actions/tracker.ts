import { IRecord, IResponseRecord } from '../../../types';
import { AppThunk } from '../init/store';
import { tracker } from '../../../api/tracker';
import { typesTrackers } from '../types/trackers';
import { authActions } from './auth';

export const trackersActions = Object.freeze({
    setNewRecord: (newRecord: IResponseRecord) => {
        return {
            payload: newRecord,
            type:    typesTrackers.SET_RECORD,
            error:   false,
        };
    },
    setRegisterRecord: (record: IResponseRecord) => {
        return {
            payload: record,
            type:    typesTrackers.GET_RECORD,
            error:   false,
        };
    },
    setUpdateRecord: (updateRecord: IResponseRecord) => {
        return {
            payload: updateRecord,
            type:    typesTrackers.UPDATE_RECORD,
            error:   false,
        };
    },
    resetRecord: () => {
        return {
            payload: '',
            type:    typesTrackers.RESET_RECORD,
            error:   false,
        };
    },
    getCurrentScore: (score: number | null) => {
        return {
            payload: score,
            type:    typesTrackers.GET_SCORE,
            error:   false,
        };
    },
    setRecord: ({ type, record }: IRecord, token: string | null): AppThunk => async (dispatch) => {
        try {
            dispatch(authActions.startFetching());
            const newTracker = await tracker.createRecord({ type, record }, token);
            dispatch(trackersActions.setNewRecord(newTracker));
        } catch (error) {
            const { message } = error as Error;
            dispatch(authActions.setError(message));
        } finally {
            dispatch(authActions.stopFetching());
        }
    },

    getRecord: (type: string, token: string | null): AppThunk => async (dispatch) => {
        try {
            dispatch(authActions.startFetching());
            const record = await tracker.getRecord(type, token);
            dispatch(trackersActions.setRegisterRecord(record));
        } catch (error) {
            const { message } = error as Error;
            dispatch(authActions.setError(message));
        } finally {
            dispatch(authActions.stopFetching());
        }
    },
    removeRecord: () => {
        return {
            type:    typesTrackers.DELETE_REPORTS,
            payload: {},
            error:   false,
        };
    },

    removeAllRecord: (token: string | null): AppThunk => async (dispatch) => {
        try {
            dispatch(authActions.startFetching());
            await tracker.removeAllRecords(token);
            dispatch(trackersActions.removeRecord());
        } catch (error) {
            const { message } = error as Error;
            dispatch(authActions.setError(message));
        } finally {
            dispatch(authActions.stopFetching());
        }
    },
    getScoreTracker: (token: string | null): AppThunk => async (dispatch) => {
        try {
            const score = await tracker.getScore(token);
            dispatch(trackersActions.getCurrentScore(score));
        } catch (error) {
            const { message } = error as Error;
            dispatch(authActions.setError(message));
        }
    },
    updateRecord: ({ type, record }: IRecord, hash: string, token: string | null): AppThunk => async (dispatch) => {
        try {
            dispatch(authActions.startFetching());
            const updateRecord = await tracker.updateRecord({ type, record }, hash, token);
            dispatch(trackersActions.setUpdateRecord(updateRecord));
        } catch (error) {
            const { message } = error as Error;
            dispatch(authActions.setError(message));
        } finally {
            dispatch(authActions.stopFetching());
        }
    },
});
