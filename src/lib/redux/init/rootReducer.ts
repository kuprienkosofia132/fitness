// Core
import { combineReducers } from 'redux';

// Reducers
import { AuthReducer as auth } from '../reducers/auth';
import { trackerReducers as tracker } from '../reducers/tracker';

export const rootReducer = combineReducers({
    auth,
    tracker,
});
