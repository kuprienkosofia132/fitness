export enum Sex {
    MALE = 'm',
    FEMALE = 'f',
}

export type ValueType = number | string | boolean;

export interface IProfile {
    fname?: string;
    lname?: string;
    email: string;
    password?: string;
    age?: number;
    sex?: string,
    height?: number;
    weight: number;
    hash?: string;
}

export interface ILogin {
    email: string;
    password: string;
}

export interface IRecord {
    type: string;
    record: ValueType;
}

export interface IRecordForSubmit {
    type?: string;
    record: ValueType;
}

export interface IResponseRecord {
    hash: string;
    value: ValueType;
}

export interface IQuestions {
    [ key: string ]: IQuestion
}

interface IQuestion {
    question: string,
    answers?: IAnswerItem[],
    type: string,
}

export interface IAnswerItem {
    answ: string | null,
    val: string | boolean | number,
}
