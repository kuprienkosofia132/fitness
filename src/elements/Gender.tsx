// Core
import { FC } from 'react';

// Styles
import Styles from '../bus/user/components/profile/styles/index.module.scss';

/* Other */
import { getToken } from '../lib/redux/selectors/auth';
import { useAppSelector } from '../lib/redux/init/store';

type Props = {
    text?: string
    typeName?: string
    setValue: Function
    watch: Function
    disabled?: boolean
};

export const Gender: FC<Props> = (props: Props) => {
    const token = useAppSelector(getToken);
    const sex = props.watch('sex');

    return (
        <div
            onClick = { () => {
                if (!token) {
                    props.setValue('sex', props.typeName);
                }
            } }
            className = { `${props.typeName === 'm' ? Styles.male : Styles.female}
            ${(sex === props.typeName || props.disabled) && `${Styles.selected}`}` } >
            <span>
                { props.text }
            </span>
        </div>
    );
};
