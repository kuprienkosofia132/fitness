import { useLocation } from 'react-router-dom';
import { book } from '../navigation/book';

export const isLoginOrRegistration = () => {
    const path = useLocation();

    return (
        path.pathname === book.registration.url || path.pathname === book.login.url
    );
};
