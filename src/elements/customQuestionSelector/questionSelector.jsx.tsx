// Other
import { useLocation } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { useEffect, useState } from 'react';

// Components
import { Base } from '../../views/base';
import { Input } from '../Input';

// Hooks
import { useUpdateRecord } from '../../hooks/useUpdateRecord';
import { useGetRecord } from '../../hooks/useGetRecord';

// Styles
import Styles from './styles/index.module.scss';
import StylesInput from '../customQuestionInput/styles/index.module.scss';
import StyleCheckBox from '../customQuestionCheckboxes/styles/index.module.scss';

/* Other */
import { useAppDispatch, useAppSelector } from '../../lib/redux/init/store';
import { trackersActions } from '../../lib/redux/actions/tracker';
import { getStatusLoading, getToken } from '../../lib/redux/selectors/auth';
import { getRecord } from '../../lib/redux/selectors/tracker';
import { IAnswerItem, IQuestions, IRecordForSubmit } from '../../types';

export const QuestionSelector = (): JSX.Element => {
    const token = useAppSelector(getToken);
    const isLoading = useAppSelector(getStatusLoading);

    const dispatch = useAppDispatch();

    const [selectedAnswer, setSelectedAnswer] = useState<string | boolean | number>('');
    const [water, setWater] = useState<string | boolean | number>(-1);

    const tracker = useAppSelector(getRecord);
    const path = useLocation();

    const itemPathArray = path.pathname.split('/');
    const itemPath: string = itemPathArray[ itemPathArray.length - 1 ];

    const { record } = useGetRecord();
    const { updateRecord } = useUpdateRecord();

    const questionItem: IQuestions = {
        breakfast: {
            question: 'Ты сегодня завтракал?',
            answers:  [
                { answ: 'Я не завтракал', val: 'none' },
                { answ: 'У меня был легкий завтрак', val: 'light' },
                { answ: 'Я очень плотно покушал', val: 'heavy' },
            ],
            type: 'selector',
        },
        lunch: {
            question: 'Ты сегодня обедал?',
            answers:  [
                { answ: 'Я не обедал', val: 'none' },
                { answ: 'Уменя был легкий обед', val: 'light' },
                { answ: 'Я очень плотно покушал', val: 'heavy' },
            ],
            type: 'selector',
        },
        dinner: {
            question: 'Ты сегодня ужинал?',
            answers:  [
                { answ: 'Я не ужинал', val: 'none' },
                { answ: 'Уменя был легкий ужин', val: 'light' },
                { answ: 'Я очень плотно покушал', val: 'heavy' },
            ],
            type: 'selector',
        },
        fruits: {
            question: 'Ты сегодня кушал фрукты?',
            answers:  [
                { answ: 'Да', val: true },
                { answ: 'Нет', val: false },
            ],
            type: 'selector',
        },
        vegetables: {
            question: 'Ты сегодня кушал овощи?',
            answers:  [
                { answ: 'Да', val: true },
                { answ: 'Нет', val: false },
            ],
            type: 'selector',
        },
        junk: {
            question: 'Ты сегодня кушал фастфуд?',
            answers:  [
                { answ: 'Да', val: true },
                { answ: 'Нет', val: false },
            ],
            type: 'selector',
        },
        coffee: {
            question: 'Ты сегодня пил кофе?',
            answers:  [
                { answ: 'Я не пил совсем', val: 'none' },
                { answ: 'Выпил 1 стакан', val: 'light' },
                { answ: 'Выпил 2 или больше стаканов', val: 'heavy' },
            ],
            type: 'selector',
        },
        steps: {
            question: 'Сколько шагов ты сегодня прошел?',
            type:     'input',
        },
        sleep: {
            question: 'Сколько часов ты сегодня спал?',
            type:     'input',
        },
        water: {
            question: 'Сколько воды ты сегодня выпил?',
            type:     'checkbox',
        },
    };

    const question = questionItem[ itemPath ];

    const form = useForm();

    useEffect(() => {
        if (tracker && question.type !== 'checkbox') {
            setSelectedAnswer(tracker.value);
            form.setValue('record', tracker.value);
        } else {
            setWater(tracker.value);
        }
    }, [tracker]);

    useEffect(() => {
        return () => {
            dispatch(trackersActions.resetRecord());
        };
    }, []);


    useEffect(() => {
        if (itemPath) {
            record(itemPath, token);
        }
    }, [itemPath]);

    const showButtons = () => {
        const cups = [];
        for (let i = 0; i < 13; i++) {
            cups.push(<button
                key = { `${water}-${i}` }
                onClick = { () => {
                    setWater(i);
                    form.setValue('record', i);
                } }
                className = { `${StyleCheckBox.element} ${water >= i && StyleCheckBox.selected}` } />);
        }

        return <>{ cups }</>;
    };

    const answers = () => {
        switch (question.type) {
            case 'selector':
                return (
                    <div className = { Styles.answers }> {
                        question.answers?.map((answer: IAnswerItem, index: number) => {
                            return (
                                <span
                                    onClick = { () => {
                                        form.setValue('record', answer.val);
                                        setSelectedAnswer(answer.val);
                                    } }
                                    key = { `${itemPath}_${index}` }
                                    className = { `${Styles.answer} ${selectedAnswer === answer.val
                                        ? `${Styles.selected}` : ''}` }>
                                    <>{ answer.answ }</>
                                </span>
                            );
                        }) }
                    </div>
                );
            case 'input':
                return (
                    <div className = { StylesInput.inputRow }>
                        <Input placeholder = 'Введите свое число' register = { form.register('record') } />
                    </div>
                );
            case 'checkbox':
                return (
                    <div className = { StyleCheckBox.elements }>
                        { showButtons() }
                        <span className = { StyleCheckBox.size }>
                            { `${(parseInt(water as string, 10) + 1) * 250} мл` }
                        </span>
                    </div>
                );
            default:
                return null;
        }
    };
    const onSubmit = form.handleSubmit((val: IRecordForSubmit) => {
        if (tracker.value) {
            updateRecord({ type: itemPath, ...val }, tracker.hash, token);
        } else {
            dispatch(trackersActions.setRecord({ type: itemPath, ...val }, token));
        }
    });

    const questionForm = () => {
        return (
            <form className = { Styles.question } onSubmit = { onSubmit }>
                <h1>{ question.question }</h1>
                { answers() }
                <button
                    disabled = { isLoading } type = 'submit'
                    className = { Styles.sendAnswer }>Ответить</button>
            </form>

        );
    };

    return <Base>{ questionForm() }</Base>;
};
