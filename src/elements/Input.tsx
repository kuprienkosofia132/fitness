// Core
import { FC } from 'react';
import { FieldError, UseFormRegisterReturn } from 'react-hook-form';

/* Other */
import { useLocation } from 'react-router-dom';
import Style from '../bus/user/components/login/styles/index.module.scss';
import StyleRegister from '../bus/user/components/profile/styles/index.module.scss';
import { book } from '../navigation/book';

interface IProps {
    placeholder: string
    label?: string
    register: UseFormRegisterReturn
    type?: string
    error?: FieldError
}

export const Input: FC<IProps> = (props) => {
    const path = useLocation();

    const input = (
        <input
            type = { props.type }
            placeholder = { props.placeholder }
            { ...props.register } />);

    return (
        <>
            <span className = 'error'>{ props.error?.message }</span>
            <div className = { path.pathname === book.login.url ? Style.inputRow : StyleRegister.inputRow }>
                <label>
                    { props.label }
                </label>
                { input }
            </div>
        </>
    );
};
