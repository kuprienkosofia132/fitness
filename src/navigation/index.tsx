// Core
import { FC, useEffect } from 'react';
import {
    Route, Routes, Navigate, useNavigate,
} from 'react-router-dom';

// Book
import { book } from './book';

// Components
import { useAppSelector } from '../lib/redux/init/store';
import { getToken } from '../lib/redux/selectors/auth';

export const RoutesComponent: FC = () => {
    const token = useAppSelector(getToken);
    const navigate = useNavigate();

    useEffect(() => {
        if (!token) {
            navigate('/login');
        }
    }, [token]);

    const routesJSX = Object
        .values(book)
        .map(({ url, page: Page }) => (
            <Route
                key = { url } path = { url }
                element = {
                    <>
                        <Page />
                    </> } />
        ));

    return (
        <>
            <Routes>
                { routesJSX }
                <Route path = '*'  element = { <Navigate to = { book.login.url } replace /> } />
            </Routes>
        </>
    );
};
