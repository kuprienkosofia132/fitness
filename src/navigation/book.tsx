// Components
import { Dashboard } from '../pages';
import { loginForm } from '../bus/user/components/login/login/loginForm';
import { QuestionSelector } from '../elements/customQuestionSelector/questionSelector.jsx';
import { Profile } from '../bus/user/components/profile/profile';
import { RegistrationPage } from '../bus/user/components/registration/forms/registration/registrationPage';

const base = '/';

export const book = Object.freeze({
    root: {
        url:  `${base}`,
        page: Dashboard,
    },
    login: {
        url:  `${base}login`,
        page: () => <>{ loginForm() }</>,
    },
    registration: {
        url:  `${base}registration`,
        page: () => <>{ RegistrationPage() }</>,
    },
    profile: {
        url:  `${base}profile`,
        page: () => <Profile />,
    },
    breakfast: {
        url:  `${base}breakfast`,
        page: () => <><QuestionSelector /></>,
    },
    coffee: {
        url:  `${base}coffee`,
        page: () => <QuestionSelector />,
    },
    dinner: {
        url:  `${base}dinner`,
        page: () => <QuestionSelector />,
    },
    fruits: {
        url:  `${base}fruits`,
        page: () => <QuestionSelector />,
    },
    junk: {
        url:  `${base}junk`,
        page: () => <QuestionSelector />,
    },
    lunch: {
        url:  `${base}lunch`,
        page: () => <QuestionSelector />,
    },
    sleep: {
        url:  `${base}sleep`,
        page: () => <QuestionSelector />,
    },
    steps: {
        url:  `${base}steps`,
        page: () => <QuestionSelector />,
    },
    vegetables: {
        url:  `${base}vegetables`,
        page: () => <QuestionSelector />,
    },
    water: {
        url:  `${base}water`,
        page: () => <><QuestionSelector /></>,
    },
});

export type BookType = typeof book;
