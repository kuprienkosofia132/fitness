import { RegistrationForm } from '../registration/forms/registration/registrationForm';
import { Base } from '../../../../views/base';
import Style from './styles/index.module.scss';
import { useDeleteRecord } from '../../../../hooks/useDeleteRecord';

export const Profile = () => {
    const { deleteDailyRecord } = useDeleteRecord();

    return (
        <Base center>
            <div className = { Style.profile }>
                <RegistrationForm title = 'Профиль' />
                <button
                    type = 'reset' onClick = { () => deleteDailyRecord() }
                    className = { Style.clearAllRecords }>Очистить все данные</button>
            </div>
        </Base>
    );
};
