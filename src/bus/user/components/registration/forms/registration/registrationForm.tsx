// Core
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { useEffect } from 'react';

// Components
import { Input } from '../../../../../../elements/Input';
import { Gender } from '../../../../../../elements/Gender';
import { book } from '../../../../../../navigation/book';

// Styles
import Styles from '../../../profile/styles/index.module.scss';

// Hooks
import { useActWithUser } from '../../../../../../hooks/useActWithUser';

/* Other */
import { IProfile } from '../../../../../../types';
import { useAppSelector } from '../../../../../../lib/redux/init/store';
import { getErrorMessage, getProfile, getToken } from '../../../../../../lib/redux/selectors/auth';
import { SignUpShape } from './config';
import { isLoginOrRegistration } from '../../../../../../elements/helpers';

interface IProps {
    title: string
}

export const RegistrationForm = (props: IProps) => {
    const authUser = useAppSelector(getProfile);
    const { signUpUser } = useActWithUser();
    const { updateUser } = useActWithUser();
    const navigate = useNavigate();
    const isLogin = isLoginOrRegistration();
    const storageToken = useAppSelector(getToken);
    const error = useAppSelector(getErrorMessage);

    useEffect(() => {
        if (storageToken && isLogin) {
            navigate(`${book.root.url}`);
        } else if (!storageToken) {
            toast.error(error);
        }
    });

    const form = useForm({
        mode:          'onTouched',
        resolver:      yupResolver(SignUpShape),
        defaultValues: authUser,
    });

    const onSubmit = form.handleSubmit(async (data: IProfile) => {
        const { hash, ...newData } = data;
        if (isLogin) {
            await signUpUser(data);
        }
        await updateUser(newData, storageToken);
    });

    const onReset = () => {
        form.reset();
    };

    return (
        <>
            <form onSubmit = { onSubmit }>
                <div className = { Styles.profile }>
                    <h1>{ props.title }</h1>
                    <div className = { Styles.gender }>
                        { authUser?.sex
                            ? <Gender
                                text = { authUser.sex === 'f' ? 'Женщина' : 'Мужчина' }
                                disabled
                                setValue = { () => {} } watch = { () => {} }
                                typeName = { authUser?.sex } />
                            : <>
                                <Gender
                                    watch = { form.watch }
                                    text = 'Женщина'
                                    typeName = 'f'
                                    setValue = { form.setValue } />
                                <Gender
                                    watch = { form.watch }
                                    text = 'Мужчина'
                                    typeName = 'm'
                                    setValue = { form.setValue } />
                            </>
                        }
                    </div>
                    <div>
                        <Input
                            placeholder = 'Введите свой email'
                            label = 'Электропочта'
                            register = { form.register('email') }
                            error = { form.formState.errors.email } />
                        <Input
                            placeholder = 'Введите свое имя'
                            label = 'Имя'
                            register = { form.register('fname') }
                            error = { form.formState.errors.fname } />
                        <Input
                            placeholder = 'Введите свою фамилию'
                            label = 'Фамилия'
                            register = { form.register('lname') }
                            error = { form.formState.errors.lname } />
                        <Input
                            placeholder = 'Введите свой пароль'
                            label = 'Пароль'
                            register = { form.register('password') }
                            error = { form.formState.errors.password } />
                        <Input
                            placeholder = 'Введите свой возраст'
                            label = 'Возраст'
                            register = { form.register('age') }
                            error = { form.formState.errors.age } />
                        <Input
                            placeholder = 'Введите свой рост'
                            label = 'Рост'
                            register = { form.register('height') }
                            error = { form.formState.errors.height } />
                        <Input
                            placeholder = 'Введите свой вес'
                            label = 'Вес'
                            type = 'number'
                            register = { form.register('weight') }
                            error = { form.formState.errors.weight } />
                    </div>
                    <div className = { Styles.controls }>
                        <button className = { Styles.clearData } onClick = { onReset }>Сбросить</button>
                        <button type = 'submit' >{ isLogin
                            ? 'Зарегестрироваться' : 'Обновить' }</button>
                    </div>
                </div>
            </form>
        </>
    );
};
