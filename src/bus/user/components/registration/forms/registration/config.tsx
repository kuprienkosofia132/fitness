// Core
import * as yup from 'yup';

export const SignUpShape = yup.object().shape({
    email:    yup.string().email('Почта должна быть настоящей').required('Это поле обязательное к заполнению'),
    fname:    yup.string().min(2, 'Минимальное количество символов - 2'),
    lname:    yup.string().min(2, 'Минимальное количество символов - 2'),
    password: yup.string().required('Это поле обязательное к заполнению'),
    age:      yup.number()
        .typeError('Используйте только цифры')
        .min(0, 'Минимальное значение - 0'),
    height: yup.number()
        .typeError('Используйте только цифры')
        .min(0, 'Минимальное значение - 0'),
    weight: yup.number().moreThan(1).positive().required('Это поле обязательное к заполнению')
        .typeError('Используйте только цифры'),
});
