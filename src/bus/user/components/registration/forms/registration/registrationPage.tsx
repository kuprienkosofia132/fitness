// Components
import { RegistrationForm } from './registrationForm';

// Styles
import Style from '../../styles/index.module.scss';
import { isLoginOrRegistration } from '../../../../../../elements/helpers';

export const RegistrationPage = () => {
    return (
        <section className = { Style.registration }>
            <div className = { Style.left }>
                <RegistrationForm title = 'Регистрация' />
            </div>
            { isLoginOrRegistration() ? <div className = { Style.right } /> : null }
        </section>
    );
};
