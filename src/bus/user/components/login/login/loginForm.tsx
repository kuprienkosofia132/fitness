// Core
import { useForm } from 'react-hook-form';
import { useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { yupResolver } from '@hookform/resolvers/yup';

// Styles
import Styles from '../styles/index.module.scss';

// Hooks
import { useActWithUser } from '../../../../../hooks/useActWithUser';

// Components
import { Input } from '../../../../../elements/Input';

/* Other */
import { ILogin } from '../../../../../types';
import { useAppSelector } from '../../../../../lib/redux/init/store';
import { getStatusLoading, getToken } from '../../../../../lib/redux/selectors/auth';
import { LoginShape } from './config';
import { Spinner } from '../../../../../elements/spinner';

export const loginForm = () => {
    const base = '/';
    const isLoading = useAppSelector(getStatusLoading);
    const token = useAppSelector(getToken);
    const navigate = useNavigate();
    const { loginUser } = useActWithUser();

    const form = useForm({
        mode:     'onTouched',
        resolver: yupResolver(LoginShape),
    });

    const onSubmit = form.handleSubmit(async (user: ILogin) => {
        await loginUser(user);
    });

    const loaderCX = <Spinner isLoading = { isLoading } />;

    useEffect(() => {
        if (token) {
            navigate('/');
        } else {
            navigate('/login');
        }
    }, [token]);

    return (
        <section className = { Styles.login }>
            <div className = { Styles.content }>
                <form onSubmit = { onSubmit }>
                    <h1>Добро пожаловать</h1>
                    <Input
                        placeholder = 'Введите свою электропочту'
                        label = 'Электропочта'
                        register = { form.register('email') }
                        error = { form.formState.errors.email } />
                    <Input
                        placeholder = 'Введите свой пароль'
                        label = 'Пароль'
                        register = { form.register('password') }
                        error = { form.formState.errors.password } />
                    <div>
                        <button type = 'submit'>Войти в систему</button>
                        <div className = { Styles.loginLink }>
                            <span>Если у вас нет аккаунта, пожалуйста </span>
                            <Link to = { `${base}registration` }>зарегистрируйтесь.</Link>
                        </div>
                    </div>
                </form>
            </div>
            { loaderCX }
        </section>
    );
};
