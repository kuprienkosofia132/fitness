import * as yup from 'yup';

export const LoginShape = yup.object().shape({
    email:    yup.string().email('Почта должна быть настоящей').required('Это поле обязательное к заполнению'),
    password: yup.string().required('Это поле обязательное к заполнению'),
});
