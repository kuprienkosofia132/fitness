// Core
import { Link } from 'react-router-dom';

// Styles
import StylesDashboard from './styles/index.module.scss';

export const DashboardFront = () => {
    const base = '/';

    const itemsLife = [
        { title: 'Добавить завтрак', description: 'Хороший завтрак очень важен', nameRoute: 'breakfast' },
        { title: 'Добавить обед', description: 'Успешные люди обедают', nameRoute: 'lunch' },
        { title: 'Добавить ужин', description: 'Лучше не ужинать вообще', nameRoute: 'dinner' },
        { title: 'Добавить активность', description: 'Пешие прогулки это минимум', nameRoute: 'steps' },
        { title: 'Добавить фрукты', description: 'Фрукты подымают настроение', nameRoute: 'fruits' },
        { title: 'Добавить овощи', description: 'Овощи очень важны', nameRoute: 'vegetables' },
        { title: 'Добавить фастфуд', description: 'Эта еда очень вредная', nameRoute: 'junk' },
        { title: 'Добавить воду', description: 'Вода это жизнь', nameRoute: 'water' },
        { title: 'Добавить сон', description: 'Спать нужно всем', nameRoute: 'sleep' },
        { title: 'Добавить кофе', description: 'Можно и без него', nameRoute: 'coffee' },
    ];

    const getBoar = (index: number) => StylesDashboard[ `category${index}` ];


    const itemLife = itemsLife.map((item, index) => {
        return (
            <Link
                key = { index }
                to = { `${base}${item.nameRoute}` }
                className = { `${StylesDashboard.link} ${getBoar(index)}` }>
                <span className = { StylesDashboard.title }>
                    { item.title }
                </span>
                <span className = { StylesDashboard.description }>
                    { item.description }
                </span>
            </Link>
        );
    });

    return (
        <>
            <div className = { StylesDashboard.dashboard }>
                <div className = { StylesDashboard.navigation }>
                    <h1>Как у тебя проходит день?</h1>
                    <div className = { StylesDashboard.items }>
                        { itemLife }
                    </div>
                </div>
            </div>
        </>
    );
};
